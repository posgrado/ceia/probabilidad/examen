import numpy as np
from scipy import stats

# Los datos del enunciado son:
A = 1
varianza = 0
sigma_w = 0.4
mu_w = 0
n = 10
alpha = 0.05

# Con lo cual es posible calcular
mu_y = A + mu_w
varianza_w = np.square(sigma_w)
var_y = varianza + varianza_w
sigma_y = np.sqrt(var_y)
Tmin = stats.t.ppf(alpha/2, df=(n - 1))
Tmax = stats.t.ppf(1-alpha/2, df=(n - 1))

# Simulamos haciendo muestreos aleatorios de a 10 muestras
y_ = []
sigmay = []
y_min = []
y_max = []
for i in range(1000):
    y = np.random.normal(mu_y, sigma_y, 10)
    y_ = np.append(y_, y.mean())
    sigmay = np.append(sigmay, y.std())
    y_min = np.append(y_min, Tmin * np.mean(sigmay) / np.sqrt(n) + A)
    y_max = np.append(y_max, Tmax * np.mean(sigmay) / np.sqrt(n) + A)

print(f'La media se encuentra entre {y_min.mean()} y {y_max.mean()}')

# Repetimos el ciclo anterior pero reemplazando A = 0
for i in range(1000):
    y = np.random.normal(mu_y, sigma_y, 10)
    y_ = np.append(y_, y.mean())
    sigmay = np.append(sigmay, y.std())
    y_min = np.append(y_min, Tmin * np.mean(sigmay) / np.sqrt(n) + 0)
    y_max = np.append(y_max, Tmax * np.mean(sigmay) / np.sqrt(n) + 0)

print(
    f'Si A = 0, entonces las muestras deberían estar entre {y_min.mean()} y {y_max.mean()}')