# La probabilidad de que gane Marcelo, si el empezó el juego, es 0,8
P_gM_eM = 0.8

# La probabilidad de que gane Marcelo, si Jerónimo empezó el juego, es 0,5
P_gM_eJ = 0.5

# Tiran 4 veces una moneda equilibrada. 
# Por lo que la probabilidad de observar número par de caras es:
P_Par = 0.5

# Y número impar:
P_Im = 0.5

# Si h > t, el juego lo empieza Jerónimo
# Por lo que la probabilidad de que empiece Jerónimo es:
P_eJ = 0.5

# Es decir, la probabilidad de que empiece Marcelo es:
P_eM = 0.5

# A su vez, la probabilidad de que empiece Jerónimo y 
# h = impar es la probabilidad de que h = 3 dentro de 
# las 4 posibilidades
P_eJ_u_Im = 1/4

# Lo mismo sucede para Marcelo con h = 1
P_eM_u_Im = 1/4

# Por ende, la probabilidad de que haya empezado Jerónimo, si
# se observó un número impar de caras
P_eJ_Im = P_eJ_u_Im / P_Im
print(f'La probabilidad de que empiece Jerónimo tal que salió un número impar de caras es: {P_eJ_Im}')

# Para el caso en que Marcelo comienza después de que salió
# un número impar de caras
P_eM_Im = P_eM_u_Im / P_Im 
print(f'La probabilidad de que empiece Marcelo tal que salió un número impar de caras es: {P_eM_Im}')

# La probablidad de que empiece Jerónimo y haya sido par el número de caras
# se reduce al caso en que los 4 eventos son cara, es decir:
P_eJ_u_Par = 1/16

# Sin embargo, para el caso en que empieza Marcelo porque hay 2 caras
# hay muchas más alternativas, exactamente
P_eM_u_Par = 7/16

# Entonces, podemos calcular 
P_eJ_Par = P_eJ_u_Par / P_Par
print(f'La probabilidad de que empiece Jerónimo tal que salió un número par de caras es: {P_eJ_Par}')

P_eM_Par = P_eM_u_Par / P_Par
print(f'La probabilidad de que empiece Marcelo tal que salió un número par de caras es: {P_eM_Par}')

# El problema es encontrar la probabilida de que h sea impar
# dado que ganó Marcelo. Esto se podría calcular de la siguiente manera:

def cal_P_Im_gM(P_gM_Im, P_Im, P_gM):
    """
    Recibe: la probabilidad de que gane Marcelo si h es impar
            la probabilidad de que h sea impar
            la probailidad de que gane Marcelo
    Devuelve la probabilidad de que haya sido impar la cantidad
    de caras si es que ganó Marcelo"""
    return (P_gM_Im * P_Im) / P_gM

# Con los datos obtenidos, la probabilidad de que gane Marcelo es:
P_gM =  P_gM_eM * P_eM_Im * P_Im + \
        P_gM_eJ * P_eJ_Im * P_Im + \
        P_gM_eM * P_eM_Par * P_Par + \
        P_gM_eJ * P_eJ_Par * P_Par

# Ahora solo falta encontrar la probabilidad de que gane Marcelo
# tal que h fue impar:
P_gM_Im = P_gM_eM * P_eM_Im + P_gM_eJ * P_eJ_Im

# Utilizamos el método definido anteriormente y finalmente
print(f'La probabilidad de que ganes Marcelos es:\n\
    P(gM) = {P_gM}\n\
Y la probabilidad de que haya sido impar el número de caras, cuando ganó Marcelo:\n\
    P(Im|gM) = {cal_P_Im_gM(P_gM_Im, P_Im, P_gM)}')

print('\n\n\n############ Simulación #####################')

# Para validar el resultado obtenido se realiza una simulación
import numpy as np

# Se pide simular 1000 veces
games = 1000
# Antes de empezar cada juego, la momeda se lanza 4 veces
coin_flips = 4

def sim_tateti(games):
    gM_eM_Par = 0
    gM_eM_Im = 0
    gM_eJ_Par = 0
    gM_eJ_Im = 0
    gJ_eM_Par = 0
    gJ_eM_Im = 0
    gJ_eJ_Par = 0
    gJ_eJ_Im = 0

    for _ in range(games):
        heads = 0
        for _ in range(coin_flips):
            result = np.random.choice(['head', 'tail'], p=[0.5, 0.5])
            if result == 'head':
                heads += 1
        
        if heads % 2 == 0:
            if heads < 3:
                winner = np.random.choice(['Marcelo', 'Jerónimo'], p=[0.8, 0.2])
                if winner == 'Marcelo':
                    gM_eM_Par += 1
                else:
                    gJ_eM_Par += 1
            else:
                winner = np.random.choice(['Marcelo', 'Jerónimo'], p=[0.5, 0.5])
                if winner == 'Marcelo':
                    gM_eJ_Par += 1
                else:
                    gJ_eJ_Par += 1
        else:
            if heads < 3:
                winner = np.random.choice(['Marcelo', 'Jerónimo'], p=[0.8, 0.2])
                if winner == 'Marcelo':
                    gM_eM_Im += 1
                else:
                    gJ_eM_Im += 1
            else:
                winner = np.random.choice(['Marcelo', 'Jerónimo'], p=[0.5, 0.5])
                if winner == 'Marcelo':
                    gM_eJ_Im += 1
                else:
                    gJ_eJ_Im += 1


    gM = gM_eJ_Im + gM_eJ_Par + gM_eM_Im + gM_eM_Par
    P_gM = gM / games
    Im_gM = gM_eM_Im + gM_eJ_Im
    P_Im_gM = (Im_gM / games) / P_gM
    return P_gM, P_Im_gM

P_gM, P_Im_gM = sim_tateti(1000)
print(f'La probabilidad de que ganes Marcelos es:\n\
    P(gM) = {P_gM}\n\
Y la probabilidad de que haya sido impar el número de caras, cuando ganó Marcelo:\n\
    P(Im|gM) = {P_Im_gM}')

