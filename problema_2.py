import numpy as np
from scipy.stats import norm
from scipy import stats
# La producción de gasolina en Neuquén tiene distribución normal
# con media y desviación estándar:
mu_nqn = 95529
sigma_nqn = 30127

# Santa Cruz también tiene distribución normal con:
mu_sc = 8268
sigma_sc = 2481

# Si consideramos que son independientes, podemos sumar
mu = mu_nqn + mu_sc
varianza = np.square(sigma_nqn) + np.square(sigma_sc)
sigma = np.sqrt(varianza)

# La probabilidad de que la producción entre ambas provincias
# sea mayor a 142925
P_x_gt_142925 = norm.sf(142925, mu, sigma)
print(f'La probabilidad de que la producción sea mayor a 142925 es:\n\
    P(x > 142925) = {P_x_gt_142925}')

# Si se necesita encontrar la probabilidad de que la producción de
# Neuquén sea 10 veces más grande que la de Santa Cruz, se debe
# considerar una nueva variable aleatoria que nace de la combinación 
# de las conocidas
mu = mu_nqn - 10 * mu_sc
varianza = np.square(sigma_nqn) + np.square(10 * sigma_sc)
sigma = np.sqrt(varianza)

# En nuestro supuesto, x = x_nqn - 10 x_sc
# Si x = 0; x_nqn = 10 x_sc
# Entonces hay que encontrar la probabilidad de que x sea mayor a 0
P_x_gt_0 = norm.sf(0, mu, sigma)
print(f'La probabilidad de que la producción de Neuquén sea 10 veces mayor a la de Santa Cruz es:\n\
    P(x_nqn > 10 x_sc) = {P_x_gt_0}')

# Hay un 95% de probabilidad de que la producción de Santa Cruz llegue a
prod_95 = norm.ppf(0.95, mu_sc, sigma_sc)
barriles = prod_95 / 0.159

print(f'Dado que los barriles almacenan 159 litros, se debe disponer de {np.ceil(barriles)}')


print('\n\n\n############ Simulación #####################')
def sim_petroleum():
    mu = mu_nqn + mu_sc
    varianza = np.square(sigma_nqn) + np.square(sigma_sc)
    sigma = np.sqrt(varianza)
    # Crea 1000 números aleatorios con distribución normal
    x = np.random.normal(mu, sigma, 1000)
    print(f'La probabilidad de que la producción sea mayor a 142925 es:\n\
        P(x > 142925) = {len(x[x > 142925])/len(x)}')

    count = 0
    for _ in range(1000):
        x_nqn = np.random.normal(mu_nqn, sigma_nqn)
        x_sc = np.random.normal(mu_sc, sigma_sc)
        if x_nqn > 10 * x_sc:
            count += 1
    
    print(f'La probabilidad de que Neuquén produzca 10 veces más que Santa Cruz es:\n\
        P(x_nqn > 10 x_sc) = {count / 1000}')
    

    x = np.random.normal(mu_sc, sigma_sc, 1000)
    prod_95 = np.percentile(x, 95)
    barriles = prod_95 / 0.159
    print(f'Dado que los barriles almacenan 159 litros, se debe disponer de {np.ceil(barriles)}')

sim_petroleum()