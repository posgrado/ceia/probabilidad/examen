import numpy as np
from scipy import stats

# La probabilidad de que salga una cara en cada evento es:
p = 1/2


num, denom = stats.binom.cdf(2, n=4, p=p).as_integer_ratio()
print(f'La probabilidad de acumular 2 caras en 4 eventos es: {num}/{denom}')